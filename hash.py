import hashlib
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--filename',  type=str)
args = parser.parse_args()

with open(args.filename, 'rb') as file:
    h = hashlib.sha256()
    h.update(file.read())
    dhash = h.hexdigest()

print(dhash)